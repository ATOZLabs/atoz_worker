import docker
import flask
import daemon

docker_client = docker.from_env()
docker_llclient = docker.APIClient()

app = flask.Flask(__name__)

mapped_volume_path = '/home/ubuntu/users_store/'

prod_env = True


@app.route('/start_container/<user_name>')
def start_container(user_name):
    try:
        user_storage = mapped_volume_path + user_name + '/'
        print("Starting a new container for user: "+user_name)
        if prod_env:
            container = docker_client.containers.run("tf-ssh",
                                                     detach=True, remove=True,
                                                     ports={'22/tcp': None, '8888/tcp': '8888', '6006/tcp': '6006'},
                                                     volumes={user_storage: {'bind': '/storage/', 'mode': 'rw'}},
                                                     runtime="nvidia")
        else:
            print('running in dev env')
            container = docker_client.containers.run("tf-ssh:localhost",
                                                     detach=True, remove=True,
                                                     ports={'22/tcp': None, '8888/tcp': '8888', '6006/tcp': '6006'},
                                                     volumes={user_storage: {'bind': '/storage/', 'mode': 'rw'}})

        res = container.exec_run("/run_jupyter.sh --allow-root --notebook-dir=/storage/notebooks/", stream=True)
        jupyter_token = ""
        for line in res.output:
            line = line.decode()
            if "?token=" in line:
                i = line.index("?token=")
                jupyter_token = line[i + 7 : i + 7 + 48]
                break

        container.exec_run("tensorboard --logdir=/storage/artifacts", detach=True)
        if prod_env:
            container.exec_run("dpkg -i  /storage/libcudnn7_7.4.2.24-1+cuda10.0_amd64.deb", detach=False)
        ssh_port = docker_llclient.port(container.id, 22)[0]['HostPort']
        jupyter_port = docker_llclient.port(container.id, 8888)[0]['HostPort']
        tensorboard_port = docker_llclient.port(container.id, 6006)[0]['HostPort']
        res = {'id': container.id, 'ssh_port': ssh_port, 'jupyter_port': jupyter_port, 'jupyter_token': jupyter_token,
               'tensorboard_port': tensorboard_port}
        print("Finish to launch docker successfully, res: "+str(res))
        return flask.jsonify(res)
    except docker.api.client.DockerException as e:
        print("Got an exception:{}".format(e))
        return flask.Response(str(e), status=500)


@app.route('/stop_container/<container_id>')
def stop_container(container_id):
    try:
        print("Stopping container %s." % container_id)
        docker_client.containers.get(container_id).stop()
        return "Container %s stopped." % container_id
    except docker.api.client.DockerException as e:
        return flask.Response(str(e), status=500)


@app.route('/download_tar/<container_id>/artifacts.tar')
def download_tar(container_id):
    try:
        print("Downloading tar from %s." % container_id)
        stream, res = docker_client.containers.get(container_id).get_archive("/storage/artifacts/")
        return flask.Response(stream, mimetype='application/x-tar')
    except docker.api.client.DockerException as e:
        return flask.Response(str(e), status=500)


@app.route('/upload_tar/<container_id>', methods=['POST'])
def upload_tar(container_id):
    try:
        if 'file' not in flask.request.files:
            return flask.Response("No file in request", status=500)
        print("Uploading tar to %s." % container_id)
        # print(flask.request.files['file'])
        docker_client.containers.get(container_id).put_archive("/datasets/", flask.request.files['file'].stream)
        return "File uploaded successfully"
    except docker.api.client.DockerException as e:
        return flask.Response(str(e), status=500)


if __name__ == '__main__':
    if prod_env:
        with daemon.DaemonContext():
            app.run(port=8081, host='0.0.0.0')
    else:
        app.run(port=8081, host='0.0.0.0')
