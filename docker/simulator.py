import random
import time

import flask
import math

app = flask.Flask(__name__)


def guid():
    def s4():
        str_hex = "%0.2X" % math.floor((1 + random.uniform(0, 1)) * 0x10000)
        return str_hex[1:]
    return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4()


@app.route('/start_container/')
def start_container():
    res = flask.jsonify({'id': guid(), 'ssh_port': 22, 'jupyter_port': 8888, 'jupyter_token': "0832419c0e2a2e5eaa2452073103ad7e2e97a9700fa54f3", 'tensorboard_port': 6006})
    print("start_container: "+str(res))
    return res


@app.route('/stop_container/<container_id>')
def stop_container(container_id):
    print("stop_container: " + str(container_id))
    return "Container %s stopped." % container_id



@app.route('/download_tar/<container_id>/artifacts.tar')
def download_tar(container_id):
    return
    try:
        print("Downloading tar from %s." % container_id)
        stream, res = docker_client.containers.get(container_id).get_archive("/artifacts/")
        return flask.Response(stream, mimetype='application/x-tar')
    except docker.api.client.DockerException as e:
        return flask.Response(str(e), status=500)


@app.route('/upload_tar/<container_id>', methods=['POST'])
def upload_tar(container_id):
    return
    try:
        if 'file' not in flask.request.files:
            return flask.Response("No file in request", status=500)
        print("Uploading tar to %s." % container_id)
        # print(flask.request.files['file'])
        docker_client.containers.get(container_id).put_archive("/datasets/", flask.request.files['file'].stream)
        return "File uploaded successfully"
    except docker.api.client.DockerException as e:
        return flask.Response(str(e), status=500)


if __name__ == '__main__':
    app.run(port=8081, host='0.0.0.0')
    print("running")
    time.sleep(100000)
