#!/bin/bash

if ! grep "FROM" "docker/tf-ssh.docker" | grep -v "^#" > /dev/null; 
then
	echo "Please run:install.sh first"
	exit 
fi

trap 'kill %1' SIGINT

# Kill previous servers 
#*************************
pkill python3

# Rerun servers 
#*************************
nohup python3 docker/main.py  >> /tmp/docker_main.log  2>&1 & 

