#!/bin/bash -xv
# ATOZ_Demo_New

# Verify that install.sh didn't alrady run
#*********************************************
if grep "FROM" "docker/tf-ssh.docker" | grep -v "^#" > /dev/null ;
then
        echo "Install already run, aborting"
        echo "Run: run.sh"
        exit
fi

#update tf-ssh.docker file 
#*********************************************
if [ $AVX_SUPPORT == "n" ]; 
then 
	sed -i "s|#\(FROM atozlabs\)|\1|" docker/tf-ssh.docker
else
	sed -i "s|#\(FROM tensorflow\)|\1|" docker/tf-ssh.docker
fi

#update main.py file 
#*********************************************
if [ $AVX_SUPPORT == "y" ]; 
then
	sed -i '/container.exec_run("dpkg.*libcudnn7/d' docker/main.py
fi

echo "Creating docker image"
pushd  docker
docker build . -f tf-ssh.docker -t tf-ssh
popd 

echo "Installing python packages"
sudo pip install flask
sudo pip install docker

echo
echo "**********************************"
echo "Running applications..."
echo "**********************************"
./run.sh
